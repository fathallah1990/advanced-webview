package download.fathalla.webviewlib;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import im.delight.android.webview.AdvancedWebView;

public class MainActivity extends AppCompatActivity  implements AdvancedWebView.Listener {
    private AdvancedWebView mWebView;
    private ProgressBar progressBar;
    private LinearLayout reloadLayout;
    private Button reloadBtn;
    private String url="https://www.google.com.eg";//put url here
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mWebView = findViewById(R.id.webview);
        mWebView.setListener(this, this);
        mWebView.loadUrl(url);
        progressBar = findViewById(R.id.progressBar);
        reloadLayout = findViewById(R.id.reloadLayout);
        reloadBtn = findViewById(R.id.reloadBtn);
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        mWebView.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebView.onDestroy();
        // ...
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

    @Override
    public void onBackPressed() {
        if (!mWebView.onBackPressed()) {
            return;
        }
        // ...
        super.onBackPressed();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPageFinished(String url) {
        progressBar.setVisibility(View.GONE);
        mWebView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        Log.d("errorcode111",errorCode+"");
        Log.d("description111",description+"");
        progressBar.setVisibility(View.GONE);
        mWebView.setVisibility(View.GONE);
        reloadLayout.setVisibility(View.VISIBLE);
        reloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reloadLayout.setVisibility(View.GONE);
                mWebView.setListener(MainActivity.this, MainActivity.this);
                mWebView.loadUrl(url);
            }
        });
        Log.d("noInterntet", "no internert");
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }
}
